import styled from 'styled-components';
import { mq } from '../utils'

const Page = styled.article``;
const StyledLinks = styled.section`
	
	align-items: flex-end;
	display: grid;
	grid-template-columns: repeat(2, 1fr);
	grid-column-gap: 20px;
	
	${mq.smUp`
		grid-template-columns: repeat(4, 1fr);
	`}

	img {
		max-width: 175px;
	}
`;

const StyledContent = styled.section``;

const withProject = ({ name, Summary = () => {}, Links = () => {} }) => WrappedComponent => {
	const WithProject = props => {
		return (
			<Page>
				<h2>{name}</h2>

				<StyledLinks>{Links.map(link => link)}</StyledLinks>

				<StyledContent>
					<Summary />
				</StyledContent>
			</Page>
		);
	};

	return WithProject;
};

export default withProject;
