import { motion } from 'framer-motion';

const withRouteTransiton = WrappedComponent => {
  const WithTransition = props => {
    return (
      <motion.div
        initial={{ opacity: 0, x: '-100vw' }}
        animate="enter"
        exit="exit"
        variants={{
          enter: {
            // scale: 1,
            x: 0,
            opacity: 1,
            transition: { duration: 0.5, ease: [0.48, 0.15, 0.25, 0.96] }
          },
          exit: {
            // scale: 0.6,
            x: '100vw',
            opacity: 0,
            transition: { duration: 0.5, ease: [0.48, 0.15, 0.25, 0.96] }
          }
        }}
      >
        <WrappedComponent {...props} />
      </motion.div>
    );
  };

  return WithTransition;
};

export default withRouteTransiton;
