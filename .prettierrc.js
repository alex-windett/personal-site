module.exports = {
	semi: true,
	singleQuote: true,
	printWidth: 80,
	arrowParens: 'always',
	ignorePath: '**/*.yaml',
	useTabs: true,
};
