import { css } from 'styled-components';

export const screenSizes = {
	small: 480,
	medium: 767,
	large: 992,
	xLarge: 1200
};

const mqSizes = {
	smDown: `(max-width: ${screenSizes.small}px)`,
	mdDown: `(max-width: ${screenSizes.medium}px)`,
	smToMd: `(min-width: ${screenSizes.small + 1}px) and (max-width: ${screenSizes.medium + 1}px)`,
	smUp: `(min-width: ${screenSizes.small}px)`,
	mdUp: `(min-width: ${screenSizes.small + 1}px)`,
	lgUp: `(min-width: ${screenSizes.large + 1}px)`,
	xlgUp: `(min-width: ${screenSizes.xLarge + 1}px)`
};

export const mq = Object.keys(mqSizes).reduce((acc, label) => {
	return label
		? {
				...acc,
				[label]: (...args) => css`
					@media screen and ${mqSizes[label]} {
						${css(...args)};
					}
				`
		  }
		: acc;
}, {});
