import React from 'react';
import styled from 'styled-components';
import { Mail } from 'styled-icons/feather';
import { Github } from 'styled-icons/boxicons-logos';
import { Gitlab } from 'styled-icons/fa-brands';
import { Strava } from 'styled-icons/fa-brands';

const Footer = styled.footer`
  grid-area: footer;
	text-align: center;
	margin: 30px;
	padding-top: 30px;
	border-top: 1px solid ${({ theme }) => theme.color};
`;

const Ul = styled.ul`
	list-style-type: none;
	padding: 0;
	display: flex;
	flex-direction: row;
	max-width: 200px;
	margin: 0 auto;
	align-items: flex-end;
`;

const Item = styled.li`
	& + & {
		margin-left: 15px;
	}
`;

export default () => (
	<Footer>
		<Ul>
			<Item>
				<a href='mailto:windettalex@gmail.com'>
					<Mail size={30} />
				</a>
			</Item>

			<Item>
				<a href='https://github.com/alex-windett' target='_blank'>
					<Github size={30} />
				</a>
			</Item>

			<Item>
				<a href='https://gitlab.com/alex-windett' target='_blank'>
					<Gitlab size={30} />
				</a>
			</Item>

			<Item>
				<a href='https://www.strava.com/athletes/7778906' target='_blank'>
					<Strava size={30} />
				</a>
			</Item>
		</Ul>
	</Footer>
);
