import React, { useState } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { lighten } from 'polished';
import { black } from '../styles';

const NavList = styled.ul`
	list-style: none;
	padding: 0 10px;
	transition: color ease-in 0.3s;
	display: inline-block;

	li {
		margin-top: 10px;
	}
`;

const Nav = styled.nav`
	grid-area: nav;
	display: inline-block;
`;

const ProjectList = styled.ul`
	transition: all ease-in-out 0.3s;
	overflow: hidden;

	height: ${({ isOpen }) => (isOpen ? 'unset' : '0')};
`;

const ProjectLink = styled.span`
	color: ${black};
	text-decoration: underline;
	cursor: pointer;

	&:hover {
		color: ${lighten(0.2, black)};
	}
`;

export default ({ theme }) => {
	const [projectsOpen, setProjectsOpen] = useState(false);

	return (
		<Nav theme={theme}>
			<NavList>
				<li>
					<Link href="/">
						<a>Home</a>
					</Link>
				</li>

				<li>
					<ProjectLink onClick={() => setProjectsOpen(!projectsOpen)}>
						Projects {projectsOpen ? '↑' : '↓'}{' '}
					</ProjectLink>

					<ProjectList isOpen={projectsOpen}>
						<li>
							<Link href="/projects/rapha">
								<a>Rapha</a>
							</Link>
						</li>

						<li>
							<Link href="/projects/freelance">
								<a>Freelance</a>
							</Link>
						</li>

						<li>
							<Link href="/projects/yld">
								<a>YLD / Make Us Proud</a>
							</Link>
						</li>

						<li>
							<Link href="/projects/big-fish">
								<a>Big Fish</a>
							</Link>
						</li>
					</ProjectList>
				</li>

				<li>
					<Link href="/cycling">
						<a>Cycling</a>
					</Link>
				</li>
			</NavList>
		</Nav>
	);
};
