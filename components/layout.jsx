import { useState } from 'react';
import styled from 'styled-components';
import { mq } from '../utils';

import { ThemeProvider } from 'styled-components';

import Nav from './nav';
import Footer from './footer';

import { black } from '../styles';

const Layout = styled.div`
	position: relative;
	min-height: 100vh;
	background-color: ${({ theme }) => theme.background};
	color: ${({ theme }) => theme.color};
	display: grid;
	marin: 0 30px;
	grid-template-areas:
		'banner'
		'nav'
		'main'
		'footer';

	${mq.smUp`
		grid-template-areas: 	'banner banner banner banner'
								'nav main main .'
								'footer footer footer footer';
	`}

	* {
		font-family: ${({ theme }) => theme.font};
	}
`;

const Wrapper = styled.main`
	grid-area: main;
	position: relative;
	min-height: calc(100vh - 110px - 164px);
	padding: 0 30px;
`;

const Banner = styled.section`
	grid-area: banner;
	text-align: center;
	width: 100vw;
	padding: 30px 0;
	position: relative;
	top: 0;
	left: 0;
	box-shadow: 0px 1px 3px black;
`;

export default ({ children }) => {
	const baseTheme = {
		background: 'transparent',
		color: black,
		font: "'Roboto', sans-serif;",
	};
	const [Theme, setTheme] = useState(baseTheme);

	return (
		<ThemeProvider theme={Theme}>
			<Layout>
				<Banner>Fanastic new things being developed</Banner>
				<Nav theme={Theme} />
				<Wrapper>{children}</Wrapper>
				<Footer />
			</Layout>
		</ThemeProvider>
	);
};
