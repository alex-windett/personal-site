import withProject from '../../hocs/withProject';

const PageLinks = [
	<a href="https://www.rapha.cc">
		<img src="/images/rapha.png" alt="Rapha Racing" />
	</a>,
];

const Summary = () => (
	<>
		<p>
			I have been working at Rapha Racing since 2017, being involved heavily
			with the development of multiple applications. Starting off with the
			redesign and redevelopment of their main e-commerce platform, I then moved
			over to the Rapha Custom project. This offered customers an end-to-end
			journey from design to management to ordering of team kit through a
			Next.js application hosted on Google Cloud Services. Since the start of
			2022 I have been leading the team on a migration to a event-driven
			microservice architecture utilizing Cloudinary and Contentful at its core
			that will be intergrated with CloudCommerce.
		</p>
		<p>
			Throughout the projects I help on the complete stack of development from
			design workshops, aiding and giving advice on GraphQL and API servers,
			creation of CI envrionemnts and pipelines to application hosting.
		</p>
	</>
);

const Page = () => {};

export default withProject({ name: 'Rapha', Links: PageLinks, Summary })(Page);
