import withProject from '../../hocs/withProject';

const PageLinks = [
	<a href="https://www.dotwatcher.cc/" alt="Dotwatcher">
		<img src="/images/dotwatcher.png" />
		<span>dotwatcher.cc</span>
	</a>,
];

const Summary = () => (
	<p>
		I am always on the lookout for new projects to work with, so if you are in
		need of some help with Shopify, Wordpress or want something completely
		unique I would love to hear from you. I am currently working with the team
		at{' '}
		<a href="https://www.dotwatcher.cc/" target="_blank">
			DoWatcher
		</a>{' '}
		with their reporting and event coverage.
	</p>
);

const Page = () => {};

export default withProject({
	name: 'Freelance and Amusings',
	Links: PageLinks,
	Summary,
})(Page);
