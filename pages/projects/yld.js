import withProject from '../../hocs/withProject';

const PageLinks = [
	<a href='https://www.yld.io/'><img src="/images/yld.png" alt="YLD"/></a>,
	<a href='https://github.com/yldio/joyent-portal'>Joyent Repo</a>,
];

const Summary = () => (
	<p>
		Working largely on a Joyent’s platform I acted as the mediator between the design process and
		the development. Work ranged from conducting user interviews and Sketch wireframes to small
		coded prototypes and a Lerna monorepo. I continued to advance my React and JS knowledge by pair
		programming with developers more junior and see as myself. I was also able to shadow senior
		backend developers providing me with a more rounded knowledge of web development.
	</p>
);

const Page = () => {};

export default withProject({ name: 'YLD / Make Us Proud', Links: PageLinks, Summary })(Page);
