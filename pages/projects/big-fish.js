import withProject from '../../hocs/withProject';

const PageLinks = [
	<a href='https://www.bigfish.co.uk/'>bigfish.co.uk</a>,
	<a href='https://www.freddiesflowers.com/'>freddiesflowers.com</a>,
	<a href='https://www.yeovalley.co.uk/'>yeovalley.co.uk</a>
];

const Summary = () => (
	<p>
		Working closely with designers, account managers and other developers I produced brand websites
		and subscription model services for the likes of Yeo Valley, Freddies Flowers and Dorset
		Cereals. Regular meetings with external stakeholders directly meant that I was often given
		responsibility for how projects should be completed. From this I introduced React into the
		companies front end stack as a way to build out a customers "dashboard" on the Freddie’s Flowers
		app.
	</p>
);

const Page = () => {};

export default withProject({ name: 'Big Fish', Links: PageLinks, Summary })(Page);
