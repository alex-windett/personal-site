import App from 'next/app';
import React from 'react';
import { Normalize } from 'styled-normalize';
import Layout from '../components/layout';

import GlobalStyles from '../styles';

import { AnimatePresence } from 'framer-motion';

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, router } = this.props;

    return (
      <AnimatePresence exitBeforeEnter>
        <Normalize />
        <GlobalStyles />
        <Layout changeColor={this.changeColor}>
          <Component {...pageProps} {...this.state} key={router.route} />
        </Layout>
      </AnimatePresence>
    );
  }
}
