import React from 'react';

const Home = () => (
	<React.Fragment>
		<h2>Alex Windett</h2>

		<p>
			I am a Senior Software Engingeer based in London with a main interest in
			JavaScript services, especially React web applications.
		</p>

		<p>
			If I’m not at my desk I'll often be found racing around a muddy field in
			cyclo-cross races or a remote part of Europe bikepacking.
		</p>

		<p>
			I always look to work with other creative developers on large projects
			which has led me to have great positions in agencies and companies around
			London. I always like to try new things and to help people out, so from
			time to time am lucky enough to have some weekend or evening work on the
			side.
		</p>

		<p>
			Currently working full time on{' '}
			<a href="https://rapha.cc" target="_blank">
				Rapha's
			</a>{' '}
			front end architecture and in my spare time I am helping{' '}
			<a href="https://dotwatcher.cc" targte="_blank">
				Dotwatcher
			</a>{' '}
			build out their ultra-endurance website. I am always looking to help out
			or talk over projects, so feel free to email me at{' '}
			<a href="mailto:alex@windett.co.uk">alex@windett.co.uk</a>.
		</p>
	</React.Fragment>
);

export default Home;
