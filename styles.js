import { createGlobalStyle } from 'styled-components';
import { lighten } from 'polished';

import Montserrat from './public/fonts/Hellvetica.woff';

export const black = '#151414';
export const blackHover = '#807c7c';
export const white = `#eaeaea`;

export default createGlobalStyle`
  @font-face {
    font-family: Helvetica Ugly;
    src: url(${Montserrat});
  }

  a {
    color: black;
    transition: color ease-in-out 0.3s;

    &:hover {
      transition: color ease-in-out 0.3s;
      color: ${lighten(0.5, 'black')}}
    }
  }
`;
